package com.example.etalytics.models;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "messages")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	private String name;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private String message;

	private Date created;

	private Date updated;

	@JsonIgnore
	@NotBlank
	private String token;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Message setName(String name) {
		this.name = name;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Message setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public Message setMessage(String message) {
		this.message = message;
		return this;
	}

	public Date getCreated() {
		return created;
	}

	public Date getUpdated() {
		return updated;
	}

	public Message setToken(String randomUUID) {
		this.token = randomUUID;
		return this;
	}

	public String getToken() {
		return token;
	}

	@PrePersist
	public void onCreate() {
		created = new Date();
	    updated = created; 
	}

	@PreUpdate
	public void onUpdate() {
		updated = new Date();
	}

}
