package com.example.etalytics.response;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.example.etalytics.models.Message;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MessageResponse {
	
	 private Long id;
	 
	 private String name; 
	 
	 private String email; 
	 
	 private String message; 
	 
	 private Date created; 
	 
	 private Date updated;
	 
	 private String token;
       
     public MessageResponse(Message message)
     { 
    	 id = message.getId(); 
    	 name = message.getName();
    	 email = message.getEmail();
    	 this.message = message.getMessage();
    	 created = message.getCreated();
    	 updated = message.getCreated();
    	 token = message.getToken();
     }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
     
     

}
