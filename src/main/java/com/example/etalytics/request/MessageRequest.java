package com.example.etalytics.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class MessageRequest {
	
	 @NotBlank
	 @Size(max=255)
	 private String name; 
	 
	 @Email
	 private String email; 
	 
	 @NotBlank
	 @Size(max=512)
	 private String message;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 
	 
	 
}
