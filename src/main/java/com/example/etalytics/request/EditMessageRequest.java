package com.example.etalytics.request;

import javax.validation.constraints.NotBlank;

public class EditMessageRequest {

	@NotBlank
	private String token;

	@NotBlank
	private String message;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
