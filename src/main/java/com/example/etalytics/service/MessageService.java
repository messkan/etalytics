package com.example.etalytics.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.etalytics.models.Message;
import com.example.etalytics.repository.MessageRepository;
import com.example.etalytics.request.MessageRequest;
import com.example.etalytics.request.EditMessageRequest;
import com.example.etalytics.exception.*;

@Service
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;

	public List<Message> findAll() {
		return messageRepository.findAll();
	}

	public List<Message> findByMaxAge(Integer age) {
		Date d = new Date();
		return messageRepository.findByMaxAge(DateUtils.addMinutes(d, -age));
	}

	public Message post(MessageRequest messageRequest) {
		Message message = new Message().setEmail(messageRequest.getEmail()).setName(messageRequest.getName())
				.setMessage(messageRequest.getMessage()).setToken(UUID.randomUUID().toString());

		messageRepository.save(message);

		return message;

	}

	public Message put(EditMessageRequest editMessageRequest, Long id) {
		Message message = messageRepository.findById(id).orElseThrow(() -> new NotFoundException("message not found"));
		if (! message.getToken().equals(editMessageRequest.getToken())) {
			throw new UnAuthorizedException("you don't have right to edit this message");
		}

		return messageRepository.save(message.setMessage(editMessageRequest.getMessage()));
	}

}
