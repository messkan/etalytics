package com.example.etalytics.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.etalytics.models.Message;

public interface MessageRepository extends JpaRepository< Message,Long>{
	 
	@Query(value = "SELECT m FROM Message m where created >= ?1")
	List<Message> findByMaxAge(Date date);
  	
}
