package com.example.etalytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtalyticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EtalyticsApplication.class, args);
	}

}
