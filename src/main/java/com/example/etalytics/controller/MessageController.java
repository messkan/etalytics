package com.example.etalytics.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.etalytics.models.Message;
import com.example.etalytics.request.EditMessageRequest;
import com.example.etalytics.request.MessageRequest;
import com.example.etalytics.response.ApiResponse;
import com.example.etalytics.response.MessageResponse;
import com.example.etalytics.service.MessageService;

@RestController
@RequestMapping("/messages")
public class MessageController {

	@Autowired
	MessageService messageService;

	@GetMapping("")
	public List<Message> findAll(@RequestParam(required = false) Integer maxAge) {
		try {
			System.out.println(maxAge);
			if (maxAge == null) {
				return messageService.findAll();
			}
			return messageService.findByMaxAge(maxAge);

		} catch (RuntimeException exception) {
			throw exception;
		}
	}

	@PostMapping("")
	public MessageResponse post(@Valid @RequestBody MessageRequest messageRequest) {
		try
		{
			Message message = messageService.post(messageRequest);
			return new MessageResponse(message);
		}catch (RuntimeException exception) {
			throw exception;
		}
		

	}

	@PutMapping("/{id}")
	public ApiResponse put(@Valid @RequestBody EditMessageRequest editMessageRequest, @PathVariable Long id) {
		try {
			messageService.put(editMessageRequest, id);
			return new ApiResponse(true, "updated");
		} catch (RuntimeException exception) {
			throw exception;
		}

	}

}
